# Secrets Sharing

## About

This is an implementation of API for a project that allows people to safely share files and/or text.

## Features

This API allows people to:

- register using email and password;
- log in using those email and password;
- upload files/texts as secrets;
- access and download secrets, uploaded by other users;
- get a list of uploaded secrets;
- delete previously uploaded secrets.

It's possible to specify whether or not secrets should be deleted after one access.

To access any secret, users should first receive a link to it from the uploader. Currently links look like this: /api/secrets/SECRET_TYPE/UNIQUE_STORED_NAME, where SECRET_TYPE is either files or texts and UNIQUE_STORED_NAME is a unique name (36 characters) given to every uploaded secret.

API uses AWS SDK for .NET to upload secrets to Yandex Object Storage.

## Usage

Before launching this project you should follow [these](https://cloud.yandex.ru/docs/storage/s3/) instructions to get an access key and create a bucket. Then, change BucketName in "appsettings.json" to the one you created, create a file ".aws-credentials" in root folder of the project and paste access key values like this:

```
AWS_ACCESS_KEY_ID="SHORTER STRING"
AWS_SECRET_ACCESS_KEY="LONGER STRING"
```