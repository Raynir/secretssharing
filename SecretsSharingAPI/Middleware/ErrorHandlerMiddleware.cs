﻿using Amazon.S3;
using Microsoft.AspNetCore.Http;
using SecretsSharingAPI.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace SecretsSharingAPI.Middleware
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";

                switch(error)
                {
                    // Custom error
                    case AppException e:
                        if (e.InnerException is AmazonS3Exception)
                            response.StatusCode = (int)HttpStatusCode.BadGateway;
                        else
                            response.StatusCode = (int)HttpStatusCode.BadRequest;
                        break;

                    // Object not found error
                    case KeyNotFoundException e:
                        response.StatusCode = (int)HttpStatusCode.NotFound;
                        break;

                    // Unhandled error
                    default:
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        break;
                }

                var result = JsonSerializer.Serialize(new { message = error?.Message });
                await response.WriteAsync(result);
            }
        }
    }
}
