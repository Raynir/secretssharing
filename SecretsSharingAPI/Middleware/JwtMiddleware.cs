﻿using Microsoft.AspNetCore.Http;
using SecretsSharingAPI.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SecretsSharingAPI.Middleware
{
    // Validates JWT token and attaches user to context,
    // if validation was successful
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;

        public JwtMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, IUserService userService, IJwtUtils jwtUtils)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            var userId = jwtUtils.ValidateToken(token);

            if (userId != null)
            {
                context.Items["User"] = userService.GetById(userId.Value);
            }

            await _next.Invoke(context);
        }
    }
}
