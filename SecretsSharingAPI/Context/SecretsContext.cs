﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SecretsSharingAPI.Models.EF;

#nullable disable

namespace SecretsSharingAPI.Context
{
    public partial class SecretsContext : DbContext
    {
        public SecretsContext()
        {
        }

        public SecretsContext(DbContextOptions<SecretsContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Secret> Secrets { get; set; }
        public virtual DbSet<SecretType> SecretTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.Email, "users_email_key")
                    .IsUnique();

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email");

                entity.Property(e => e.Passhash)
                    .IsRequired()
                    .HasColumnName("passhash");
            });

            modelBuilder.Entity<Secret>(entity =>
            {
                entity.ToTable("secrets");

                entity.HasIndex(e => e.StoredFilename, "secrets_stored_filename_key")
                    .IsUnique();

                entity.Property(e => e.SecretId)
                    .HasColumnName("secret_id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.IsOnetimeAccess).HasColumnName("is_onetime_access");

                entity.Property(e => e.OriginalFilename)
                    .IsRequired()
                    .HasColumnName("original_filename");

                entity.Property(e => e.SecretTypeId).HasColumnName("secret_type_id");

                entity.Property(e => e.StoredFilename)
                    .IsRequired()
                    .HasColumnName("stored_filename");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Secrets)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("secrets_user_id_fkey");

                entity.HasOne(d => d.SecretType)
                    .WithMany(p => p.Secrets)
                    .HasForeignKey(d => d.SecretTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("secrets_secret_type_id_fkey");
            });

            modelBuilder.Entity<SecretType>(entity =>
            {
                entity.HasKey(e => e.TypeId)
                    .HasName("secret_types_pkey");

                entity.ToTable("secret_types");

                entity.HasIndex(e => e.TypeName, "secret_types_type_name_key")
                    .IsUnique();

                entity.Property(e => e.TypeId)
                    .HasColumnName("type_id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.TypeName)
                    .IsRequired()
                    .HasColumnName("type_name");
            });

            // Initial data
            var secretTypes = new List<SecretType>
            {
                new SecretType { TypeId = 1, TypeName = "File" },
                new SecretType { TypeId = 2, TypeName = "Text" }
            };

            modelBuilder.Entity<SecretType>().HasData(secretTypes);

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
