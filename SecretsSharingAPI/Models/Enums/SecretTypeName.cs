﻿namespace SecretsSharingAPI.Models.Enums
{
    public enum SecretTypeName
    {
        File = 1,
        Text = 2
    }
}
