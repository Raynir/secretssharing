﻿using SecretsSharingAPI.Models.EF;

namespace SecretsSharingAPI.Models.Requests.Secrets
{
    public class SecretUploadResponse
    {
        public Secret SecretInfo { get; set; }
        public string AccessUrl { get; set; }
    }
}
