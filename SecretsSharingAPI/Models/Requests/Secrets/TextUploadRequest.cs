﻿using SecretsSharingAPI.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace SecretsSharingAPI.Models.Requests.Secrets
{
    public class TextUploadRequest
    {
        [Required]
        public string TextSecret { get; set; }

        public bool IsOnetimeAccess { get; set; } = false;

        public int SecretTypeId { get; set; } = (int)SecretTypeName.Text;
    }
}
