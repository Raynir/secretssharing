﻿using Microsoft.AspNetCore.Http;
using SecretsSharingAPI.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace SecretsSharingAPI.Models.Requests.Secrets
{
    public class FileUploadRequest
    {
        [Required]
        public IFormFile FileSecret { get; set; }

        public bool IsOnetimeAccess { get; set; } = false;

        public int SecretTypeId { get; set; } = (int)SecretTypeName.File;
    }
}
