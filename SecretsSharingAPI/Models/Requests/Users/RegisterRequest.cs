﻿using System.ComponentModel.DataAnnotations;

namespace SecretsSharingAPI.Models.Requests.Users
{
    public class RegisterRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
