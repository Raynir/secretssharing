﻿using System.ComponentModel.DataAnnotations;

namespace SecretsSharingAPI.Models.Requests.Users
{
    public class AuthenticateResponse
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
    }
}
