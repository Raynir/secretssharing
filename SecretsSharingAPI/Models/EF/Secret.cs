﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

#nullable disable

namespace SecretsSharingAPI.Models.EF
{
    public partial class Secret
    {
        public int SecretId { get; set; }
        public string OriginalFilename { get; set; }
        public string StoredFilename { get; set; }
        public bool IsOnetimeAccess { get; set; }
        public int SecretTypeId { get; set; }
        public int UserId { get; set; }

        [JsonIgnore]
        public virtual User User { get; set; }
        [JsonIgnore]
        public virtual SecretType SecretType { get; set; }
    }
}
