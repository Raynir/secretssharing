﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

#nullable disable

namespace SecretsSharingAPI.Models.EF
{
    public partial class User
    {
        public User()
        {
            Secrets = new HashSet<Secret>();
        }

        public int UserId { get; set; }
        public string Email { get; set; }
        public string Passhash { get; set; }

        public virtual ICollection<Secret> Secrets { get; set; }
    }
}
