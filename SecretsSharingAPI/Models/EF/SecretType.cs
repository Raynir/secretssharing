﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

#nullable disable

namespace SecretsSharingAPI.Models.EF
{
    public partial class SecretType
    {
        public SecretType()
        {
            Secrets = new HashSet<Secret>();
        }

        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public virtual ICollection<Secret> Secrets { get; set; }
    }
}
