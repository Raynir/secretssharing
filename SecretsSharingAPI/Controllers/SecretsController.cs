﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using SecretsSharingAPI.Attributes;
using SecretsSharingAPI.Context;
using SecretsSharingAPI.Exceptions;
using SecretsSharingAPI.Models.EF;
using SecretsSharingAPI.Models.Enums;
using SecretsSharingAPI.Models.Requests.Secrets;
using SecretsSharingAPI.Services;

namespace SecretsSharingAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SecretsController : ControllerBase
    {
        private readonly ISecretService _secrets;
        private readonly IFileStorageService _storage;

        public SecretsController(ISecretService secretService, IFileStorageService fileStorageService)
        {
            _secrets = secretService;
            _storage = fileStorageService;
        }

        // GET: api/secrets
        [HttpGet]
        public ActionResult<IEnumerable<Secret>> GetUserSecrets()
        {
            User user = GetCurrentUser();
            return Ok(_secrets.GetAllUserSecrets(user.UserId));
        }

        // GET: api/secrets/files/{secret}
        [AllowAnonymous]
        [HttpGet("files/{secretName}")]
        public async Task<IActionResult> GetFileSecret(string secretName)
        {
            var secret = _secrets.GetSecret(secretName);
            if (secret.SecretType.TypeId != (int)SecretTypeName.File)
                throw new AppException("Secret not found");

            var file = await _storage.GetSecretFile(secretName);

            if (secret.IsOnetimeAccess)
            {
                await _secrets.RemoveSecretOnAccess(secretName);
                await _storage.DeleteSecret(secretName);
            }

            return File(file, GetMimeType(secret.OriginalFilename), secret.OriginalFilename);
        }

        // GET: api/secrets/texts/{secret}
        [AllowAnonymous]
        [HttpGet("texts/{secretName}")]
        public async Task<ActionResult<string>> GetTextSecret(string secretName)
        {
            var secret = _secrets.GetSecret(secretName);
            if (secret.SecretType.TypeId != (int)SecretTypeName.Text)
                throw new AppException("Secret not found");

            var text = await _storage.GetSecretText(secretName);

            if (secret.IsOnetimeAccess)
            {
                await _secrets.RemoveSecretOnAccess(secretName);
                await _storage.DeleteSecret(secretName);
            }

            return text;
        }

        // POST: api/secrets/files
        [HttpPost("files")]
        public async Task<ActionResult<Secret>> UploadFile([FromForm] FileUploadRequest model)
        {
            User user = GetCurrentUser();

            var secret = await _secrets.AddFileSecret(model, user.UserId);
            await _storage.UploadFile(model, secret.StoredFilename);

            // One of the requirements is for users to receive the URL for the uploaded secret.
            // While there is a Location header already attached from CreatedAtAction, 
            // I decided to include the URL in the body, just in case
            var response = new SecretUploadResponse
            {
                SecretInfo = secret,
                AccessUrl = Url.Action("GetFileSecret", "Secrets", new { secretName = secret.StoredFilename }, "https")
            };

            return CreatedAtAction("GetFileSecret", new { secretName = secret.StoredFilename }, response);
        }

        // POST: api/secrets/texts
        [HttpPost("texts")]
        public async Task<ActionResult<Secret>> UploadText(TextUploadRequest model)
        {
            User user = GetCurrentUser();

            var secret = await _secrets.AddTextSecret(model, user.UserId);
            await _storage.UploadText(model, secret.StoredFilename);

            var response = new SecretUploadResponse
            {
                SecretInfo = secret,
                AccessUrl = Url.Action("GetTextSecret", "Secrets", new { secretName = secret.StoredFilename }, "https")
            };

            return CreatedAtAction("GetTextSecret", new { secretName = secret.StoredFilename }, response);
        }

        // DELETE: api/secrets/{secret}
        [HttpDelete("{secretName}")]
        public async Task<IActionResult> DeleteSecret(string secretName)
        {
            User user = GetCurrentUser();

            await _secrets.RemoveSecret(secretName, user.UserId);
            await _storage.DeleteSecret(secretName);

            return NoContent();
        }


        private User GetCurrentUser()
        {
            if (HttpContext.Items["User"] is not User user)
                throw new AppException("Couldn't retrieve current user from context");
            return user;
        }

        private string GetMimeType(string fileName)
        {
            const string DefaultContentType = "application/octet-stream";

            var provider = new FileExtensionContentTypeProvider();

            if (!provider.TryGetContentType(fileName, out string contentType))
            {
                contentType = DefaultContentType;
            }

            return contentType;
        }
    }
}
