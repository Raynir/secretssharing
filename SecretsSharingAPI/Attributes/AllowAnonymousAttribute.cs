﻿using System;

namespace SecretsSharingAPI.Attributes
{
    // Custom AllowsAnonymous attribute, for consistency's sake
    [AttributeUsage(AttributeTargets.Method)]
    public class AllowAnonymousAttribute : Attribute
    {
    }
}
