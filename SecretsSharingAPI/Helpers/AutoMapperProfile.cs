﻿using AutoMapper;
using SecretsSharingAPI.Models.EF;
using SecretsSharingAPI.Models.Requests.Secrets;
using SecretsSharingAPI.Models.Requests.Users;

namespace SecretsSharingAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            // User -> AuthenticateResponse
            CreateMap<User, AuthenticateResponse>();
            // RegisterRequest -> User
            CreateMap<RegisterRequest, User>();
            // TextUploadRequest -> Secret
            CreateMap<TextUploadRequest, Secret>();
            // FileUploadRequest -> Secret
            CreateMap<FileUploadRequest, Secret>();
        }
    }
}
