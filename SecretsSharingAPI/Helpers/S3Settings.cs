﻿namespace SecretsSharingAPI.Helpers
{
    public class S3Settings
    {
        public string ServiceURL { get; set; }
        public string BucketName { get; set; }
    }
}
