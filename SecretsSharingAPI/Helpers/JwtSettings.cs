﻿namespace SecretsSharingAPI.Helpers
{
    // Used for accessing applications settings using DI
    public class JwtSettings
    {
        public string Key { get; set; }
    }
}
