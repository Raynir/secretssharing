﻿using SecretsSharingAPI.Models.EF;

namespace SecretsSharingAPI.Services
{
    public interface IJwtUtils
    {
        public string GenerateToken(User user);
        public int? ValidateToken(string token);
    }
}
