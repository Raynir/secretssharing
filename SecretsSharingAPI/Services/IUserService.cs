﻿using SecretsSharingAPI.Models.EF;
using SecretsSharingAPI.Models.Requests.Users;

namespace SecretsSharingAPI.Services
{
    public interface IUserService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        User GetById(int id);
        void Register(RegisterRequest model);
    }
}
