﻿using AutoMapper;
using SecretsSharingAPI.Context;
using SecretsSharingAPI.Models.EF;
using SecretsSharingAPI.Models.Requests.Secrets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SecretsSharingAPI.Services
{
    public class SecretService : ISecretService
    {
        private readonly SecretsContext _context;
        private readonly IMapper _mapper;

        public SecretService(SecretsContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // Save data on new secret file to database
        public async Task<Secret> AddFileSecret(FileUploadRequest model, int userId)
        {
            var secret = _mapper.Map<Secret>(model);

            secret.OriginalFilename = model.FileSecret.FileName;
            secret.StoredFilename = Guid.NewGuid().ToString();
            secret.UserId = userId;

            _context.Secrets.Add(secret);
            await _context.SaveChangesAsync();

            return secret;
        }

        // S3 stores any string as files, so their "original" filenames
        // are the same as their secret filenames 
        public async Task<Secret> AddTextSecret(TextUploadRequest model, int userId)
        {
            var secret = _mapper.Map<Secret>(model);

            secret.StoredFilename = Guid.NewGuid().ToString();
            secret.OriginalFilename = secret.StoredFilename;
            secret.UserId = userId;

            _context.Secrets.Add(secret);
            await _context.SaveChangesAsync();

            return secret;
        }

        public IEnumerable<Secret> GetAllUserSecrets(int userId)
        {
            return _context.Secrets.Where(s => s.UserId == userId).ToList();
        }

        // Secrets are searched by their unique names
        public Secret GetSecret(string secretName)
        {
            var secret = _context.Secrets
                .Where(s => s.StoredFilename == secretName)
                .Include(s => s.SecretType)
                .FirstOrDefault();
            if (secret == null) throw new KeyNotFoundException("Secret not found");
            return secret;
        }

        // Only owners of secrets can delete them
        public async Task RemoveSecret(string secretName, int userId)
        {
            var secret = _context.Secrets
                .Where(s => s.StoredFilename == secretName &&
                            s.UserId == userId)
                .FirstOrDefault();
            if (secret == null) throw new KeyNotFoundException("Secret not found");

            _context.Secrets.Remove(secret);
            await _context.SaveChangesAsync();
        }

        // ... or if it's a one-time access file, then anyone can
        public async Task RemoveSecretOnAccess(string secretName)
        {
            var secret = _context.Secrets
                .Where(s => s.StoredFilename == secretName)
                .FirstOrDefault();
            if (secret == null) throw new KeyNotFoundException("Secret not found");

            _context.Secrets.Remove(secret);
            await _context.SaveChangesAsync();
        }
    }
}
