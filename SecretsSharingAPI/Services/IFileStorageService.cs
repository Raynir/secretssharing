﻿using SecretsSharingAPI.Models.Requests.Secrets;
using System.IO;
using System.Threading.Tasks;

namespace SecretsSharingAPI.Services
{
    public interface IFileStorageService
    {
        Task<byte[]> GetSecretFile(string secretName);
        Task<string> GetSecretText(string secretName);
        Task UploadFile(FileUploadRequest model, string secretName);
        Task UploadText(TextUploadRequest model, string secretName);
        Task DeleteSecret(string secretName);
    }
}
