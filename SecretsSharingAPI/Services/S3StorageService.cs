﻿using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Options;
using SecretsSharingAPI.Exceptions;
using SecretsSharingAPI.Helpers;
using SecretsSharingAPI.Models.Requests.Secrets;
using System.IO;
using System.Threading.Tasks;

namespace SecretsSharingAPI.Services
{
    public class S3StorageService : IFileStorageService
    {
        private readonly S3Settings _s3Settings;
        private readonly IAmazonS3 _s3Client;

        // Yandex Cloud is used for cloud storage, 
        // so we have to configure ServiceURL
        public S3StorageService(IOptions<S3Settings> s3Settings)
        {
            _s3Settings = s3Settings.Value;

            var config = new AmazonS3Config()
            {
                ServiceURL = _s3Settings.ServiceURL
            };

            _s3Client = new AmazonS3Client(config);
        }

        public async Task<byte[]> GetSecretFile(string secretName)
        {
            try
            {
                var file = new MemoryStream();

                var request = new GetObjectRequest
                {
                    BucketName = _s3Settings.BucketName,
                    Key = secretName
                };

                using var response = await _s3Client.GetObjectAsync(request);
                response.ResponseStream.CopyTo(file);

                return file.ToArray();
            }
            catch (AmazonS3Exception ex)
            {
                throw new AppException("Failed to get secret text from cloud storage", ex);
            }
        }

        public async Task<string> GetSecretText(string secretName)
        {
            try
            {
                var request = new GetObjectRequest
                {
                    BucketName = _s3Settings.BucketName,
                    Key = secretName
                };

                // ResponseStream can be read to get the text from uploaded files
                using var response = await _s3Client.GetObjectAsync(request);
                using var reader = new StreamReader(response.ResponseStream);

                return reader.ReadToEnd();
            }
            catch (AmazonS3Exception ex)
            {
                throw new AppException("Failed to get secret file from cloud storage", ex);
            }
        }

        public async Task UploadFile(FileUploadRequest model, string secretName)
        {
            try
            {
                using var stream = new MemoryStream();

                model.FileSecret.CopyTo(stream);

                // To upload a file, request has to have an InputStream
                var request = new PutObjectRequest
                {
                    InputStream = stream,
                    Key = secretName,
                    BucketName = _s3Settings.BucketName
                };

                await _s3Client.PutObjectAsync(request);
            }
            catch (AmazonS3Exception ex)
            {
                throw new AppException("Failed to upload secret to cloud storage", ex);
            }
        }

        public async Task UploadText(TextUploadRequest model, string secretName)
        {
            try
            {
                // To upload a string, request has to have ContentBody
                // It will still be uploaded as a file, though
                var request = new PutObjectRequest
                {
                    BucketName = _s3Settings.BucketName,
                    Key = secretName,
                    ContentBody = model.TextSecret
                };

                await _s3Client.PutObjectAsync(request);
            }
            catch (AmazonS3Exception ex)
            {
                throw new AppException("Failed to upload secret to cloud storage", ex);
            }
        }

        public async Task DeleteSecret(string secretName)
        {
            try
            {
                var request = new DeleteObjectRequest
                {
                    BucketName = _s3Settings.BucketName,
                    Key = secretName
                };

                await _s3Client.DeleteObjectAsync(request);
            }
            catch (AmazonS3Exception ex)
            {
                throw new AppException("Failed to delete secret from cloud storage", ex);
            }
        }
    }
}
