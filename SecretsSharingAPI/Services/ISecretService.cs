﻿using SecretsSharingAPI.Models.EF;
using SecretsSharingAPI.Models.Requests.Secrets;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SecretsSharingAPI.Services
{
    public interface ISecretService
    {
        IEnumerable<Secret> GetAllUserSecrets(int userId);
        Secret GetSecret(string secretName);
        Task<Secret> AddFileSecret(FileUploadRequest model, int userId);
        Task<Secret> AddTextSecret(TextUploadRequest model, int userId);
        Task RemoveSecret(string secretName, int userId);
        Task RemoveSecretOnAccess(string secretName);

    }
}
