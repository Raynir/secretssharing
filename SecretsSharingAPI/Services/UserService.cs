﻿using AutoMapper;
using SecretsSharingAPI.Context;
using SecretsSharingAPI.Exceptions;
using SecretsSharingAPI.Models.EF;
using SecretsSharingAPI.Models.Requests.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using BC = BCrypt.Net.BCrypt;

namespace SecretsSharingAPI.Services
{
    public class UserService : IUserService
    {
        private readonly SecretsContext _context;
        private readonly IJwtUtils _jwtUtils;
        private readonly IMapper _mapper;

        public UserService(SecretsContext context, IJwtUtils jwtUtils, IMapper mapper)
        {
            _context = context;
            _jwtUtils = jwtUtils;
            _mapper = mapper;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = _context.Users.SingleOrDefault(u => u.Email == model.Email);

            // Verify user
            if (user == null || !BC.Verify(model.Password, user.Passhash))
                throw new AppException("Incorrect username or password");

            // Generate response on success
            var response = _mapper.Map<AuthenticateResponse>(user);
            response.Token = _jwtUtils.GenerateToken(user);
            return response;
        }

        public User GetById(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null) throw new KeyNotFoundException("User not found");
            return user;
        }

        public void Register(RegisterRequest model)
        {
            // Check whether or not email is available
            if (_context.Users.Any(u => u.Email == model.Email))
                throw new AppException($"Email {model.Email} is already taken");

            // Create and save new user
            var user = _mapper.Map<User>(model);
            user.Passhash = BC.HashPassword(model.Password);
            _context.Users.Add(user);
            _context.SaveChanges();
        }
    }
}
