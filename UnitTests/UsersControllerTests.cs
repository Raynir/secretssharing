using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SecretsSharingAPI.Controllers;
using SecretsSharingAPI.Exceptions;
using SecretsSharingAPI.Models.Requests.Users;
using SecretsSharingAPI.Services;
using System;
using Xunit;

namespace UnitTests
{
    public class UsersControllerTests
    {
        private readonly Mock<IUserService> _userServiceStub = new();
        private readonly Random _rand = new();

        [Fact]
        public void Authenticate_WithValidRequest_ReturnsOkWithResponse()
        {
            // Arrange
            var response = GetAuthenticateResponse();
            _userServiceStub.Setup(s => s.Authenticate(It.IsAny<AuthenticateRequest>()))
                .Returns(response);
            var controller = new UsersController(_userServiceStub.Object);

            // Act
            var result = controller.Authenticate(new AuthenticateRequest());

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result);
            okResult.Value.Should().BeEquivalentTo(response);
        }

        [Fact]
        public void Authenticate_WithInvalidRequest_ThrowsAppException()
        {
            // Arrange
            _userServiceStub.Setup(s => s.Authenticate(It.IsAny<AuthenticateRequest>()))
                .Throws(new AppException("Incorrect username or password"));
            var controller = new UsersController(_userServiceStub.Object);

            // Act
            Action act = () => controller.Authenticate(new AuthenticateRequest());

            // Assert
            var ex = Assert.Throws<AppException>(act);
            Assert.Equal("Incorrect username or password", ex.Message);
        }

        [Fact]
        public void Register_WithValidRequest_ReturnsOkWithResponse()
        {
            // Arrange
            var response = new { message = "Registration successful" };
            _userServiceStub.Setup(s => s.Register(It.IsAny<RegisterRequest>()))
                .Verifiable();
            var controller = new UsersController(_userServiceStub.Object);

            // Act
            var result = controller.Register(GetRegisterRequest());

            // Assert
            Mock.VerifyAll();
            var okResult = Assert.IsType<OkObjectResult>(result);
            okResult.Value.Should().BeEquivalentTo(response);
        }

        [Fact]
        public void Register_WithTakenEmailInRequest_ThrowsAppException()
        {
            // Arrange
            var request = GetRegisterRequest();
            _userServiceStub.Setup(s => s.Register(It.IsAny<RegisterRequest>()))
                .Throws(new AppException($"Email {request.Email} is already taken"));
            var controller = new UsersController(_userServiceStub.Object);

            // Act
            Action act = () => controller.Register(request);

            // Assert
            var ex = Assert.Throws<AppException>(act);
            Assert.Equal($"Email {request.Email} is already taken", ex.Message);
        }

        private AuthenticateResponse GetAuthenticateResponse()
        {
            return new AuthenticateResponse
            {
                UserId = _rand.Next(1000),
                Email = $"user{_rand.Next(100_000)}@example.com",
                Token = "Token"

            };
        }

        private RegisterRequest GetRegisterRequest()
        {
            return new RegisterRequest
            {
                Email = $"user{_rand.Next(100_000)}@example.com",
                Password = $"{_rand.Next(1_000, 100_000)}"
            };
        }
    }
}
